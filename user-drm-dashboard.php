<?php
/**
 * Template Name: user DRM dashboard
 */

get_header(vibe_get_header());
?>

<?php
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
?>




<script>
$(document).ready(function(){
  $(".top-u-avtar").click(function(){
    $(".page-template.page-template-user-drm-dashboard #vibe_bp_login").show();
  });
});
</script>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/_inc/css/user-drm-dashboard.css"/>
<div class="row user-drm-head">
 <div class="col-sm-12 user-drm-head-inner">


  <div class="col-sm-3 user-drm-logo">
    <h1>Dotprofiles</h1>
  </div>

  <div class="col-sm-4 user-search">
   <div class="search-container">
    <form action="/action_page.php">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
  </div>

  <div class="col-sm-5 user-drm-head-icons">
    <ul>
     <li><a href="#"><i class="fa fa-home" style="color: #01ff00;"></i></a></li>
     <li><a href="#"><i class="fa fa-bell" style="color: #ffff00;"></i></a></li>
     <li><a href="#"><i class="fa fa-envelope" style="color: #fff;"></i></a></li>
     <li><a href="#"><i class="fa fa-question-circle" style="color: #01ff00;"></i></a></li>
     <li><a class="top-u-avtar" style="color:#ffff;">
<?php if(is_user_logged_in()){
  echo get_avatar( $current_user->ID, 32 );
}else{ ?>
<i class="fa fa-user-circle" style="color: #fff;"></i>
<?php } ?>
<?php
if($current_user->user_login){
  echo $current_user->user_login;
}else{
  echo "Username";
}
?></a></li>
    </ul>
  </div>


 </div>
</div>


<div class="row user-drm-content">
  <div class="col-sm-12 user-drm-inner-content">
    <div class="col-sm-2 user-drm-left-side-bar">
      <div class="col-sm-12 left-side-bar-inner">

        <div class="user-image-left">

<?php if(is_user_logged_in()){
  echo get_avatar( $current_user->ID, 80 );
}else{ ?>
<img src="<?php echo get_template_directory_uri() ?>/_inc/images/user.png">
<?php } ?>
<icon>
        </div>

        <div class="details-user-drm">
          <h4>Details <span><i class="fa fa-edit" ></i></span></h4>
           <ul>
             <li>
<?php if($current_user->user_firstname){ ?>

 <span><?php echo $current_user->user_firstname;?></span>

 <span><?php echo $current_user->user_lastname;?></span>

<?php }else{ ?>
First name Surname

<?php }  ?></li>
             <li>Professional / Student</li>
             <li>Institute Name</li>
             <li>
<?php if(get_user_meta( $user_id, 'billing_state', true )){ ?>

 <span><?php echo get_user_meta( $user_id, 'billing_state', true ); ?></span>

<?php }else{ ?>
Location / Country
<?php }  ?>
</li>
  </ul>
        </div>

        <div class="explore-user-drm">
            <h4>Explore</h4>
            <ul>
             <li><a href="https://www.dotprofiles.com/blog/"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon1.png"></span>Latest Feeds</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon2.png"></span>Intrest</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon3.png"></span>Top Post</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon4.png"></i></span>Info Tech</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon5.png"></i></span>Physics</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon6.png"></span>Computers</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon7.png"></span>Friends list</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon8.png"></span>Sports</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon9.png"></span>Event</a></li>
             <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon10.png"></span>On This Day</a></li>
             
            </ul>
        </div>


      </div>
    </div>


    <div class="col-sm-7 user-drm-middle-bar">
      <div class="user-drm-middle-bar-inner">
        <div class="user-drm-gallery">
          <h4><span>Gallery <i class="fa fa-window-restore"></i></span></h4>
          <ul>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon11.png"/></span><br>Docs</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon12.png"/></span><br>Vedios</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon13.png"/></span><br>Career</a></li>
           <li><a href="http://www.dotprofiles.com/members/user/dashboard/"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon14.png"/></span><br>Dashboard</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon15.png"/></span><br>Job Arena</a></li>
          </ul>
        </div>

      </div>
      

<div class="latest-blog">
<span class="latest-feed">Latest Feeds</span>  
<?php
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1
    );

    $post_query = new WP_Query($args);
if($post_query->have_posts() ) {
  while($post_query->have_posts() ) {
    $post_query->the_post();
    ?>
<div> 

<div>
<a href="<?php the_permalink();?>"><h2><?php the_title(); ?></h2></a>
<div>
<span class="post-by"><b>By </b><?php echo get_the_author(); ?></span>
<span class="post-date"><?php echo get_the_date( 'Y-m-d' ); ?></span>
</div>
<p><?php the_excerpt(); ?></p>
</div>

<div>
<a href="<?php the_permalink();?>"><?php the_post_thumbnail(); ?></a>
</div>
</div>
    <?php
  }
}
?>

</div>
      
    </div>


    <div class="col-sm-3 user-drm-right-side-bar">
      <div class="user-drm-right-side-inner">
        <div class="user-drm-actions">
          <h4>Actions<span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon22.png"/></span></h4>
          <ul>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon16.png"/></span><br>Publish</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon17.png"/></span><br>Go live</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon18.png"/></span><br>Promote</a></li>
          </ul>

          <ul>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon19.png"/></span><br>Demos</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon20.png"/></span><br>Join Collages</a></li>
           <li><a href="#"><span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon21.png"/></span><br>Search Place</a></li>
          </ul>
        </div>
        </div>

        <div class="user-drm-likes">
         <div class="user-drm-likes-inner">
           <ul>
             <li>Likes</li>
             <li>Views</li>
             <li>Post</li>
           </ul>

           <h1>156</h1>
           <h2>5 New Like this week</h2>
         </div>
        </div>


        <div class="user-drm-actions1">
          <div class="user-drm-actions1">
            <h4>Professional chat<span><img src="<?php echo get_template_directory_uri() ?>/_inc/images/icon23.png"></span></h4>
            <ul>
             <li><a href="#"><span><i class="fa fa-user-circle"></i></span>Contact 1</a></li>
             <li><a href="#"><span><i class="fa fa-user-circle"></i></span>Contact 2</a></li>
             <li><a href="#"><span><i class="fa fa-user-circle"></i></span>Contact 3</a></li>
             <li><a href="#"><span><i class="fa fa-user-circle"></i></span>Contact 4</a></li>
             <li><a href="#"><span><i class="fa fa-user-circle"></i></span>Contact 5</a></li>
             <li><a href="#"><span><i class="fa fa-user-circle"></i></span>Contact 6</a></li>
            </ul>

            
          </div>

        </div>
      </div>

      </div>
    </div>
  </div>
</div>



<?php
get_footer(vibe_get_footer());
?>